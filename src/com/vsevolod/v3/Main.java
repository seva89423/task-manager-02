package com.vsevolod.v3;

import com.vsevolod.v3.constant.TerminalConst;

public class Main {
    public static void main(String[] args) {
		System.out.println("Welcome to the task manager!");
		parseArgs(args);
	}

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        if(TerminalConst.HELP.equals(arg)) showHelp();
        if(TerminalConst.ABOUT.equals(arg)) showAbout();
        if(TerminalConst.VERSION.equals(arg)) showVersion();
	}

	private static void showHelp() {
		System.out.println("[HELP]");
		System.out.println(TerminalConst.HELP+" - Show developer info");
		System.out.println(TerminalConst.VERSION+"version - Show version info");
		System.out.println(TerminalConst.ABOUT+"help - Show display commands");
	}
	
    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
	}
    
    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
	}
}
