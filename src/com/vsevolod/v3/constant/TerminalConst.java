package com.vsevolod.v3.constant;

public class TerminalConst {
    public static final String HELP = "help";
    public static final String ABOUT = "about";
    public static final String VERSION = "version";
}
